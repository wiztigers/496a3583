module.exports = {
  pwa: {
    name: 'Pount',
    themeColor: '#F5A623',
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? `/${process.env.CI_PROJECT_NAME}/`
    : '/',
};
